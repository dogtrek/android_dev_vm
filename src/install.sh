#!/bin/bash
##
##
##
##
##
##


#PACKAGES="dialog sudo snapd openbox openbox-menu lightdm virtualbox-guest-additions-iso"


PACKAGES_BUILD="dialog"
PACKAGES_RUNTIME="sudo"

if [[ "$(uname -v)" != *"Debian"* ]]
then
	echo "This does not appear to be a Debian distro..\n
	The script is currently only intended to work on Debian VM's..Quitting.."
	exit 1
else
	#install needed packages etc.
	locale-gen
	apt update
	apt upgrade
	apt --assume-yes install $PACKAGES_RUNTIME $PACKAGES_BUILD
fi


dialog_backtitle_prefix="\Zb\Z6Duck-Hunt Pr0's Super-Duper Android Studio Installer!\Zr"

### END DIALOG CONFIG

echo 

dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --title 'Hello there..' --yes-label 'Yes!' --no-label 'Not Now!' --yesno "Let's get going with coding some kewl Android Apps! \n\n Ready?.." 10 34
YN_START=$?

if [ $YN_START -gt 0 ]
then
	clear
	echo "Ok.. Quitting.."
	exit $YN_START
fi

### ASK FOR USERNAME

# open fd
exec 3>&1
INPUT_USERNAME=$(dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --inputbox "Choose a username:" 10 40 2>&1 1>&3)
# close fd
exec 3>&-


### ASK USER TO SET PASSWORD
#pass_ask_return_value=255
function ask_password_set()
{
	pass1="123"
	pass2="321"

	# open fd
	exec 3>&1
	pass1=$(dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --insecure --passwordbox "Set a login password:" 10 40 2>&1 1>&3)
	# close fd
	exec 3>&-

	# open fd
	exec 3>&1
	pass2=$(dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --insecure --passwordbox "Confirm the password:" 10 40 2>&1 1>&3)
	# close fd
	exec 3>&-
}

ask_password_set
while [ "$pass1" != "$pass2" ]
do
	dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --msgbox "The two passwords were not identitical!\nPlease try again.." 10 40  
	ask_password_set
done
INPUT_PASSWORD=$pass1

## create the user

if [[ "$(uname -v)" != *"Debian"* ]]
then
	echo "This does not appear to be a Debian distro..\n"
	exit 1
else
	echo "Creating user $INPUT_USERNAME, and setting password!"
	useradd -m -U -G sudo $INPUT_USERNAME
	echo “$INPUT_USERNAME:$INPUT_PASSWORD” | chpasswd
fi

## install the desktop etc.
if [[ "$(uname -v)" != *"Debian"* ]]
then
	echo "This does not appear to be a Debian distro..\n"
	exit 1
else
	apt --assume-yes install lightdm openbox openbox-menu konsole
fi

## install virtualbox guest additions
if [[ "$(uname -v)" != *"Debian"* ]]
then
	echo "This does not appear to be a Debian distro..\n"
	exit 1
else
	## install virtualbox guest additions
	dialog --cr-wrap --colors --backtitle "$dialog_backtitle_prefix" --msgbox "Ready to install VirtualBox Guest Additions\n\nThis might take a few minutes. So please wait until it's done." 10 40  
	echo "Installing VirtualBox Guest Additions.."
	apt --assume-yes install gcc make perl virtualbox-guest-additions-iso linux-headers-$(uname -r)
	VIRTUALBOX_ISO=$(find / -name '*.iso')
	echo $VIRTUALBOX_ISO
	mkdir -p /mnt/vboxiso
	mount -o loop -r $VIRTUALBOX_ISO /mnt/vboxiso
	echo "$VIRTUALBOX_ISO mounted at /mnt/vboxiso"
	cd /mnt/vboxiso
	./VBoxLinuxAdditions.run --nox11
	umount /mnt/vboxiso
	apt --assume-yes --purge autoremove gcc make perl virtualbox-guest-additions-iso linux-headers-$(uname -r)
	rm -rf /mnt/vboxiso
fi


exit 1
